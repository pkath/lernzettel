# TODO
* Übungszettel
* Scheduling Algorithmen durchgehen
* Übungsblatt 2: Scheduling: Nicht-Optimalität von LST bei Konflikten
* Übung zur Nicht-Optimalität von LST bei Konflikten
um Prozessoren
* Laboraufgabe 2
* Beispiel Waschmaschinenluke: Einstufung in Risikograf nach EN 954-1
* Übungsblatt 4: Fehlerbaumanalyse
* Laboraufgabe 6: Risikoanalyse und zuverlässige
* Beispiel: Projekt Sichere Eisenbahnsteuerung
* Übungen am ende der folien z.b. bei kapitel 5 folie 14



# Reaktive Systeme
* Einordnung reaktiver Systeme
	* transformatorische Systeme 
		* Sortierprogramm 
		* überführen eine Menge von Eingabedaten in Ausgabedaten
und terminieren dann
	* interaktive Systeme
		* 
Textverarbeitung, Datenbankserver
		* reagieren kontinuierlich mit ihrer Umgebung
		* können den Ablauf weitgehend selbst kontrollieren
		* Reaktionen können durch Ressourcenengpässe verzögert werden
	* reaktive Systeme
		* Motorsteuerung
		* reagieren kontinuierlich mit ihrer Umgebung
		* Ablauf und Tempo durch die Umgebung kontrolliert
* Charakterisierung von Arten reaktiver Systeme
	* diskrete Anwendungen
		* reagieren auf diskrete Eingangsereignisse
		* produzieren diskrete Ausgangsereignisse
		* Betätigen eines Schalters 
		* Ausschalten eines Motors
		* kontroll(fluss)orientiert / zustandsorientiert
	* kontinuierliche Anwendungen
		* produzieren kontinuierlich Ausgangsdaten
		* aus kontinuierlich eintreffenden Eingangsdaten
		* signalverarbeitende Systeme
		* daten(fluss)orientiert
* Besondere Herausforderungen bei reaktiven Systemen für diskrete Anwendungen 
	* Nebenläufigkeit
		* Ein- und Ausgabegrößen gleichzeitig bedienen
		* Nur 1 Prozessor
	* Abbruch
		* Zu beobachtende Abbruchbedingung
		* Für "dauernd" laufendes System
		* Problem: konsistenter Zustand bei Abbruch
	* Unterbrechung
		* Muss aktiv für Dringendes unterbrochen werden
		* Problem: Wiederherstellung und Nebenläufigkeit
	* Race Conditions
		* Folge der Nebenläufigkeit
		* Folge: nicht-deterministisches Verhalten
		* Semaphore/Monitore verhindern dies
* Ein Muster für einen Lösungsansatz in Software
	* Eingabewerte ermitteln
und in internen Eingabevariablen ablegen
	* interne Ausgabevariablen anhand der (nun festen) Eingabevariablen berechnen
	* interne Ausgabevariablen ausgeben
	* Vorteile: 
		* Reihenfolge der Berechnung der internen Ausgabevariablen egal
		* keine verschränkten Zugriffe
		* konsistenter Zustand bei Abbruch (Run-to-Completion-Semantik nach Eingabe-Ereignis)
		* durch gröbere Zeitgranularität weniger Nicht-Determinismus
	* Beachten: 
		* Berechnungen müssen schnell sein
		* immer noch Nicht-Determinismus
* Auffrischung: Endliche Automaten / Schaltwerke
		* Schaltnetz
			* Die Ausgabevariablen hängen von den aktuellen Eingabevariablen ab.
		* Schaltwerk
			* Die Ausgabevariablen hängen von den aktuellen Eingabevariablen und deren Vorgeschichte ab.
	* Modellieren von Schaltwerken 
		* Endliche Automaten
			* ![Alternativer Text](eauto.png)

		* Zustände eines Systems
			* Zustände der Eingabevariablen
			* Zustände des zu bauenden Systems
			* Zustände der Ausgabevariablen
	* Implementieren von Schaltwerken
		* Huffman-Normalform?
 	* Mehr zur Darstellung von Modellen von Schaltwerken 
 		* Mealy-Automat
			* ein Endlicher Automat mit:
 			* f:Z×X→Y
 		* Moore-Automat
			* ein Endlicher Automat mit:
			* f:Z→Y
			* Ausgabefunktion f : hängt nicht von Eingabe ab
		* Darstellungsformen für Automaten
			* Zustandsgraph
				* Mealy-Typ
					* ![Alternativer Text](Mealy-Typ.png)
				* Moore-Typ
					* ![Alternativer Text](Moore-Typ.png)
 
			* Tabelle
				* ![Alternativer Text](tabellen.png)
			* „regulärer Ausdruck“, . . . 
			* spezielle VHDL-Konstrukte
			* Programmcode, z.B. in C
 	* Ein wenig zum Testen von Schaltwerken
 		* Testfolge
			* eine geordnete Liste von Paaren aus Eingabevektor ⃗x und erwartetem Ausgabevektor ⃗y

				* Eingabevektoren: nacheinander an Testling angelegt 
				*erwartete Ausgabevektoren: jeweils erwartete Reaktion
 	* __TODO__ Übungsblatt 1: Testfolge für endlichen Automaten

# Echtzeitsysteme
## Einleitung
* Was bedeutet „Echtzeit“?
	* nicht: „besonders schnell“ 
 	* nur: „sicher rechtzeitig“
	* daher meist: ziemlich langsam -> b/c Prozessorauslastung

* Worst-Case-Ausführungszeit
	* Worst-Case-Ausführungszeit
		* die Programmlogik (Kontrollflussgraph),
		* die Eingabedaten (Auswirkungen auf Schleifendurchlaufzahlen etc.),
		* den Compiler (Optimierungsstufe) und
		* die Architektur und Taktfrequenz des Ausführungsrechners (Ausführungsgeschwindigkeit unter Berücksichtigung von Cache- und Pipelining-Effekten).
	* bestimmbar: obere Schranke der WCET angeben
## Definitionen der Grundbegriffe
### Jobs und Prozessoren
* Definition Job und Task 
	* Job: 
		* Eine Aufgabe, die als Einheit vom System eingeplant und ausgeführt wird.
		* Bsp: Zustandsübergang Endlicher Automat, Übertragung Datenpaket
	* Task:
		* Eine Menge zusammengehöriger Jobs, die eine Systemfunktion realisiert
		* regelmäßige Übertragung von Datenpaketen
* Definition Prozessor
	* Prozessor
		* Eine Ressource, die für die Ausführung eines Jobs benötigt wird, und die bei der Ausführung des Jobs Zeit verbraucht.
		* BSP: CPU, Netzwerk, HDD

### Freigabezeitpunkt, Periode, Phase
* Definition Freigabezeitpunkt eines Jobs
	* Freigabezeitpunkt (release time)
	* Der Zeitpunkt, ab dem der Job ausgeführt werden kann.
	* BSP
		* ein Regler für einen Ofen
		* Ausführung startet nach Initialisierung zur Zeit 0 s
* Definition Periode und Phase eines Tasks
	* Periode
		* Der Zeitabstand zwischen den Freigabezeitpunkten der Jobs des Tasks.
	* Phase
 		* Der Freigabezeitpunkt des ersten Jobs des Tasks.

### Deadline, Zeitbedingung
* Definition Deadline eines Jobs
	* Der Zeitpunkt, zu dem die Ausführung des Jobs fertig gestellt sein muss.
* Weitere Definitionen
	* Antwortzeit (response time)
		* Die Dauer vom Freigabezeitpunkt bis zur Fertigstellung.
	* relative Deadline
		* Die maximal zulässige Antwortzeit.
	* absolute Deadline
		* Der Freigabezeitpunkt plus die relative Deadline.
	* Zeitbedingung (timing constraint)
		* Eine Bedingung für das Zeitverhalten des Jobs.

### Harte und weiche Zeitbedingungen
* Definition über die Kritikalität
	* Harte Zeitbedingung/Deadline
		* Katastrophale Folgen, wenn nicht erfüllt.
	* Weiche Zeitbedingung/Deadline
		* Unerwünschte Folgen, wenn nicht erfüllt.	
* Definition über den Nutzen verspäteter Ergebnisse
	* Verspätung eines Jobs (tardiness)
		* falls zu spät: Fertigstellungszeitpunkt minus absolute Deadline sonst: Null
	* Harte Zeitbedingung/Deadline
		* Nutzen sinkt schlagartig, falls Verspätung > 0 (Nutzen kann sogar stark negativ werden)
	* Weiche Zeitbedingung/Deadline
		* Nutzen sinkt (graduell) um so mehr, je größer Verspätung
* Definition über deterministische vs. probabilistische Zeitbedingungen 
	* Harte Zeitbedingung/Deadline 
		* Job darf nie Deadline verpassen.
	* Weiche Zeitbedingung/Deadline
		*Job darf Deadline nur mit bestimmter, niedriger Wahrscheinlichkeit verpassen.
* Definition über den Nachweis der Gültigkeit (nach Liu)
	* Harte Zeitbedingung/Deadline
		* Ein Nachweis der Gültigkeit der Zeitbedingung ist erforderlich.
	* Weiche Zeitbedingung/Deadline
		* Ein Nachweis der Gültigkeit der Zeitbedingung ist nicht erforderlich, oder es ist nur eine statistische Zeitbedingung.

## Gesamtmodell eines Echtzeitsystems
* Lastmodell -> was? 
* Ressourcen-Modell -> womit?
* Scheduling-Algorithmus -> wann?

### Lastmodell
* Zeiteigenschaften von Lasten
	* Jitter des Freigabezeitpunkts
		* Wenn genauer Freigabezeitpunkt unbekannt: Zeitspanne, in der die Freigabe geschieht
	* Aperiodischer Job
		* Konkreter Freigabezeitpunkt ist komplett unbekannt, und Deadline ist weich.
	* Sporadischer Job
		* Konkreter Freigabezeitpunkt ist komplett unbekannt, und Deadline ist hart.
* Einige weitere Eigenschaften von Lasten
	* Job-Präzedenzen
	* Datenabhängigkeiten
	* zeitliche Abhängigkeiten
	* Präemptivität
	* Kritikalität

### Ressourcen-Modell
* Prozessoren und Ressourcen 
	* Prozessor: aktive Ressource
		* (hat Geschwindigkeit, bzw. benötigt Zeit)
	* „Ressource“: passive Ressource 
		* (hat keine Geschwindigkeit)
* Eigenschaften von Ressourcen
	* Präemptivität (der Ressource)
		* Zeitweise unterbrechung d. Ressource

### Scheduling-Algorithmus
* Korrekter Schedule
	* höchstens ein Job pro Prozessor zu jeder Zeit
	* höchstens ein Prozessor pro Job zu jeder Zeit
	* kein Job vor Freigabezeitpunkt eingeplant
	* gesamte eingeplante Prozessorzeit für einen Job ist gleich max. bzw. tatsächliche Ausführungszeit (je nach Algorithmus)
	* alle Präzedenz- und Ressourcen-Bedingungen erfüllt
* Durchführbarer Schedule und optimaler Scheduling-Algorithmus
	* Durchführbarer Schedule 
		* Korrekter Schedule, der alle Deadlines einhält.
	* Optimaler Scheduling-Algorithmus
		* Findet stets einen durchführbaren Schedule, wenn er existiert.

## Häufig verwendete Ansätze für Echtzeit-Scheduling
### Zeitgesteuerte Verarbeitung
* Einführung: Zeitgesteuerte Verarbeitung 
	* für harte Echtzeit meist verwendet
	* alle wesentlichen Parameter der Jobs fest und bekannt
	* kompletter Schedule vorab berechnet
	* Realisierung oft: Hardware-Timer löst periodisch Ticks aus Scheduler wird genau durch Ticks geweckt und aktiviert dann neue Jobs 
	* so leichter Nachweis der Zeitbedingungen für harte Echtzeit
	* wenig Rechenaufwand zur Laufzeit
* Round-Robin-Verarbeitung
	* für time-sharing-Anwendungen meist verwendet
	* Realisierung oft:
		* FIFO-Schlange bereiter Jobs
		* feste, kurze Zeitscheiben
		* wenn Job nach Zeitscheibe nicht fertig: ans FIFO-Ende
	* wenig Startverzögerung für Job
	* verteilt Rechenzeit gleichmäßig auf bereite Jobs
	* Runde: ein Durchlauf, bis Job wieder dran
* Gewichtete Round-Robin-Verarbeitung
	* Gewicht eines Jobs: wie oft in einer Runde dran
	* einige Jobs so schneller fertig
	* z.B. in Hochgeschwindigkeitsdatennetzen verwendet

### Prioritätsgesteuerte Verarbeitung
* Einführung: Prioritätsgesteuerte Verarbeitung 
	* lässt nie eine Ressource absichtlich unausgelastet
	* Scheduler wird aktiv, wenn Job bereit wird oder Job fertig wird („ereignisgesteuert“)
	* für weiche Echtzeit- und Nicht-Echtzeit-Anwendungen meist verwendet
* Beispiele für prioritätsgesteuerte Algorithmen 
	* FIFO – First In First Out
 	* LIFO – Last In First Out
	* SETF – Shortest Execution Time First 
	* LETF – Longest Execution Time First
	* (Gewichtetes) Round-Robin
* Earliest Deadline First (EDF) Algorithmus
	* ist prioritätsgesteuert
	* Priorität eines Jobs höher, wenn Deadline früher
	* optimal, wenn:
		* Präemption möglich
		* keine Ressourcen-Konflikte zwischen Jobs
		* nur ein Prozessor
* Least Slack Time (LST) Algorithmus 
	* für sporadische Jobs
	* prioritätsgesteuert
	* Schlupf (engl. Slack):
		* Deadline minus aktuell frühestmögliche Fertigstellung (umgangssprachlich: „Luft in der Planung“, „Zeitpuffer“)
	* Priorität eines Jobs höher, wenn Schlupf (Slack) kleiner (d.h. wenn Planung für den Job „enger“)
	* ebenfalls optimal, wenn:
		* Präemption möglich
		* keine Ressourcen-Konflikte zwischen Jobs 
		* nur ein Prozessor
* Scheduling ohne/mit Präemption
	* __TODO__
	* Nachteil von LST mit Präemption: oft: sehr häufige Task-Wechsel
* Scheduling auf mehrere Prozessoren 
	* EDF selbst mit Präemption nicht optimal auf mehreren Prozessoren
	* LST würde hier die bessere Lösung liefern, ist aber auch nicht optimal
* Scheduling mit Migration
	* dynamisches Scheduling
	* Bei multi Prozessoren: 
	* anderer Prozessor bearbeitet Job nach Unterbrechung weiter
	* evtl. hilfreich bei Prozessoren mit ungleichen Eigenschaften (z.B. Geschwindigkeit)
* Übungsblatt 2: Scheduling: Nicht-Optimalität von LST bei Konflikten

### Verhalten prioritätsgesteuerter Systeme
* Beispiel für anormales Verhalten prioritätsgesteuerter Systeme
	* __TODO__ 
	* Gesamt-Schedule kann evtl. schlechter werden, wenn kürzere Ausführungszeiten, mehr Prozessoren oder weniger Abhängigkeiten

### Weitere Aspekte
* Scheduling unter Überlast
	* Überlast: kein durchführbarer Schedule mehr möglich
	* oft gewünscht: „relativ“ gute Lösung
	* optimale Algorithmen wie EDF, LST werden plötzlich sehr schlecht
* Scheduling mit Ressourcen-Restriktionen
	* macht Analyse wesentlich komplexer
	* __TODO__ 

## Nachweis harter Echtzeiteigenschaften

* Der Begriff der längstmöglichen Antwortzeit
	* Die längstmögliche Zeit ab einer Änderung der Eingangssignale, bis die zugehörige Reaktion der Ausgangssignale sichtbar ist.
	* Anwendung als Zeitbegriff
* Praktische Bestimmung der längstmöglichen Ausführungszeit
	* durch erschöpfendes Ausprobieren -> Problematisch
	* Lösung : vollständige Pfadabdeckung 
		* Software-Muster für reaktive Systeme mit Hauptschleife und Trennung von Eingabe, Verarbeitung und Ausgabe 
		* nach 1 Runde: Ausgabe ist garantiert geschehen 
		* nur nötig: WCET für 1 Runde bestimmen
	 	* alle Berechnungspfade nun endlich (= 1 Runde) 
	 	* nur endliche Anzahl von Berechnungspfaden
	 	* konstruierte Testfälle: führen jeden möglichen Berechnungspfad einmal
 	* vollständige Anweisungsabdeckung (für ein Stück C-Code) 
		* Jede Anweisung wird mindestens einmal ausgeführt.
 	* vollständige Verzweigungsabdeckung (für ein Stück C-Code) 
 		* Jede Verzweigung wird mindestens einmal genommen.
 		* Sollte Anweisungsabdeckung beinhalten
 	* vollständige Pfadabdeckung (für ein Stück C-Code)
 		* Jede Kombination von Verzweigungen wird mindestens einmal ausgeführt.
 		* Umfasst Verzweigungsabdeckung
	* Zusammenfassung: 
		* Einzelzeiten aufaddieren 
		* Summe evtl. größer als WCET
		* Grund: WCET der Code-Teile evtl. in verschiedenen Pfaden, und diese Pfad-Kombination nicht erreichbar
		* Summe: ist obere Schranke für die WCET kein Problem, falls obere Schranke ≤ Deadline
		* im Werkzeug: Zeiten für einzelne C-Funktionen getrennt gemessen
* Praktische Bestimmung der längstmöglichen Antwortzeit
	* Polling nur einmal/Schleifenperiode
	* worst-case: verzögerung um eine Hauptschleifenperiode
	* obere Schranke für Reaktionszeit (WCRT) = 2× obere Schranke für Berechnungszeit (WCET)
* Laboraufgabe 2: Nachweis harter Echtzeiteigenschaften (der Fahrstuhlsteuerung)
	* __TODO__ 

# Software-Engineering für eingebettete Systeme
## Modularisierung und Geheimnisprinzip
### Der Begriff des Moduls
* Schreibzeitmodule vs. Linkzeitmodule
	* Schreibzeitmodule
		* leicher zu verändern/aufzuteilen
		* vor dem kompilieren/ausführen
	* Linkzeitmodule
		* seperat kompiliert 
		* vor dem ausführen
* Was bestimmt dabei jeweils die Aufteilung in Module
	* Schreibzeitmodule
		* (fachlicher/inhaltlicher)zuammenhang für programmierer
		* verständlichkeit
		* einfacher zu ändern
	* Linkzeitmodule
		* doppelte namen
		* kompilier und linkzeit

* Folgen der Vermischung der verschiedenen Arten von Modulen
	* __TODO__ ???

### Beispiel: Zerlegung eines KWIC-Systems in Module
* Einführung in die KWIC-Aufgabe
	* Key Words In Context
* Ideen für eine Zerlegung in Module
* Eine „konventionelle“ Zerlegung in Module 
* Einige wahrscheinliche Änderungen
* Parnas’ Zerlegung in Module
* Vergleich der beiden Zerlegungen in Module
### Kriterien für ein Zerlegen in Module
* Die Kriterien
* Das Geheimnis eines Moduls
* Einige konkrete Kriterien
* Schnittstellen zwischen Modulen
* Modulstruktur
* Übungsbeispiel: Projekt Sichere Eisenbahnsteuerung



# Sicherheitsrelevante Systeme (im Sinne von Safety)
## Methoden zur Risikoanalyse
### Grundbegriffe
* Begriffsdefinitionen
	* Ausfall (failure)
		* Ein gelieferter Dienst entspricht nicht länger seiner Spezifikation.
	* Fehlerzustand (error):
		* ein Designfehler oder
		* eine Abweichung vom gewünschten Zustand
	* Störung (fault):
		* ein Ereignis, das ein System in einen ungewünschten Zustand versetzt
* Hierarchische Betrachung
	* jedes System: aus Subsystemen zusammengesetzt
	* Ausfall ebene i -> Fehlerzustand übergeordnete ebene i-1

### Risikograf
* Risikograf nach nach EN 954-1
	* ![Alternativer Text](Risikograf.png)
	* ![Alternativer Text](risiko_bew.png)
	* Kategorien
		* B
			* Normen beachten
			* grundlegende Sicherheitsprinzipien beachten 
			* hinreichend robust auslegen
		* 1
			* nur bewährte Bauteile
			* nur bewährte Sicherheitsprinzipien
	* __FEHLT NOCH WAS, ABER ...
	* __TODO__ Beispiel Waschmaschinenluke: Einstufung in Risikograf nach EN 954-1

### FMEA
__ TODO RAUS??__

### Fehlerbaumanalyse (FTA)
* Grundidee
	* FMEA: Analysiert systematisch die Details
	* FTA komplementär dazu:
		* Fängt bei globalen Ausfall-Ereignissen an
		* (Beispiel: „Zwei Züge kollidieren“)und geht schrittweise zu den detaillierten Ursachen zurück (Beispiel: „Weiche falsch gestellt“, „Bremse defekt“, . . . )
	* FTA: Bestimmen der kritischsten Komponenten und entsprechend Ändern des Entwurfs oder der Komponenten
	* Möglichkeit, Ziel zu erreichen: genug Redundanz sicherstellen
	* Vorteil gegenüber FMEA: Abhängigkeiten zwischen Fehlern werden betrachtet
* Begriffsdefinitionen
	* Cut-Set
		* eine Menge von Basis-Ereignissen, die zusammen einen Top-Level-Ausfall bewirken kann
	* minimales Cut-Set
		* ein kleinstes Cut-Set
	* Single-Point-Ausfall (single-point of failure)
		* ein Cut-Set mit nur einem Basis-Ereignis
* verschiedene Arten von Syntax für eine FTA
	* ![Alternativer Text](fta1.png)
	* ![Alternativer Text](fta2.png)
	* Nur AND und OR wichtig
* Beispiel: Bremsflüssigkeitswarnlampensystem
 	* ![Alternativer Text](warn.png)
 	* ![Alternativer Text](warn2.png)
 	* Raute-Symbol: Ereignis wird nicht bis zur Quelle zurückverfolgt
* Fehlerbäume mit Wahrscheinlichkeiten
	* Kann Wahrscheinlichkeiten an Fehlerbaum-Knoten schreiben
	* Problem: Zahlenbasis
* Übungsblatt 4: Fehlerbaumanalyse
* Laboraufgabe 6: Risikoanalyse und zuverlässige Systemarchitektur am Beispiel des Fahrradtachometers

## Hardware- und Software-Systemarchitekturen
### Sicherheitsarchitekturen
* 1-aus-1-Struktur
	* ![Alternativer Text](1-aus-1.png)
	* Standardstruktur für Steuer- und Regelungssysteme
	* Bsp: Not-Aus-Taster (Öffner)
	* einkanalige Struktur
	* für beliebige Schaltungen dieser Struktur: 
	* bei Versagen eines beliebigen Bauteils:
	* nicht mehr sicher nicht mehr verfügbar
	* HFT: 0
* Definition Hardware Fault Tolerance (HFT)
	* Die Anzahl beliebig kombinierbarer Fehler, die nicht zum Versagen führen.
* 1-aus-2-Struktur
	* ![Alternativer Text](1-2.png)
	* zweikanalige Struktur
	* Aktor nur betrieben, wenn beide Kanäle zustimmen
	* Aktor oft nur einkanalig
	* sonst sicherer Zustand
	* HFT-Wert: 1

* 1-aus-2-D-Struktur (1-aus-2 mit Diagnose)
	* ![Alternativer Text](1-aus-2-D.png)
	* Verarbeitungslogik jedes Kanals überwacht Eingangsschaltung,Verarbeitungslogik des anderen Kanals und Aktor
	* intern diagnostizierbare zweikanalige Struktur
	* Erkennung des irrenden Kanals oft nicht möglich
	* dauerhafte Abschaltung bei erkanntem Fehler
* Fail-Safe-Logik (für 1-aus-2-Struktur)
	* ![Alternativer Text](Fail-Safe-Logik.png)
	* Failsafe logik erkennt unterschiedliche signale 
		* muss einfach aufgebaut sein
		* darf nicht selbst versagen können
* 2-aus-3-Struktur
	* ![Alternativer Text](3aus1.png)
		* 3 Kanäle parallel
		* Verarbeitungslogik-Einheiten überwachen sich gegenseitig 
		* 1 Fehler in jedem der Kanäle erkennbar
		* möglich: Fail-Safe-Logik schaltet defekten Kanal aus
		* wählen, wenn längere Verfügbarkeit n
		ach Fehlerfall nötig
* Fehlerbaum mit mehrkanaligen Strukturen
	* Beispiel verinnerlichen
### Architekturen für Logikeinheiten
* Zentralprozessor mit Watchdog
	* Watchdog erkennt ausfall 
		* Ausfall CPU
		* Deadlock
	* SW setzt Watchdog zurück, falls nicht Watchdog-> sicherer Zustand
* Sicherheitsüberwachung bzw. Sicherheitsschicht
	* Extra Gerät
	* Muss Einfach und robust sein, parallele Berechnung
	* Abschalten bei groben Abweichungen
* Zweikanaligkeit mit homogener Redundanz
	* häufig verwendet
	* Hardware-Struktur homogen gedoppelt
	* Annahme: Mikrocontroller-Design & oder Betriebssystem „betriebsbewährt“
	* noch aufwändiger in der Entwicklung: diversitäre Hardeware-Redundanz
	* analog: Software ggf. auch diversitär (siehe folgende Folie)
* Einkanalige Struktur mit Softwarediversität
	* gegen Deadlocks oder andere Programmierfehler
	* Ergebnis von zwei verschieden geschriebenen Programmen berechnet und in Speicher abgelegt
	* Failsafe vergleicht Ergebnisse im Speicher

### Beispiel: Projekt Sichere Eisenbahnsteuerung
* Einordnen der Architektur des Projekts
	* __TODO__

## Wie sicher ist sicher genug?
### Risiko-Nutzen-Analyse und die Alternativen
* Utilitaristischer Ansatz
	* „durch Abwägen von Risiko und Nutzen kann man zu Entscheidungen kommen“
* Risikoabschätzung
	* eine Technik, um Wahrscheinlichkeit und Schwere eines Risikos vorab zu quantifizieren
	* Probleme
		* Keine belastbare Zahlen
		* Modelle berücksichtigen nur erwartete Ereignisse
		* Technik ist umstritten
* Akzeptables Risiko
	* Gruppe mit Risiko, Gruppe mit Nutzen
	* Starker Lobby-Einfluss
	* Kosten/Nutzen 
* Alternativer Ansatz „optimales Risiko“
	* minimiert Summe aller unerwünschten Folgen
	* Optimalitätskriterium: zusätzliche Kosten für weitere Risikoverkleinerung = gesamtgesellschaftliche Kosten dieses Risiko-Deltas
	* Ges. Kosten und Risiko müssen nicht berechnet werden
	* mehr gesamtgesellschaftliche Sicht
	* immer noch konkrete Zahlen für Wahrscheinlichkeiten nötig 
	* immer noch konkrete Geldwerte nötig
* Alternativer Ansatz „normale Unfälle“
	* Kriterien:
		* Schwere eines Schadens
		* Nutzen im Vergleich zu den Alternativen
	* Ansatz ohne Berechnung von Wahrscheinlichkeiten
	* Ziel: Unfälle mit vielen Toten vermeiden und Hochrisikotechnologien möglichst trotzdem nutzen
* Alternativer Ansatz „Unfallrate nicht erhöhen“
	* neue Technologie: keine höhere Unfallrate als Vorgängertechnologie
	* Annahme: Vorgängertechnologie ist von Öffentlichkeit akzeptiert
	* Wie relative Unfallrate der neuen Technologie vorab bestimmen?
	* Problem:
		* Kostensenkung bei gleichem Risiko
		* Kostenerhöhung bei größerem Risiko
* Bewertung der Ansätze
	* keine befriedigende Methode zur Entscheidungsfindung
	* ethische Dilemmata oft unvermeidlich

# Betriebssysteme für eingebettete Systeme

## Echtzeit-Betriebssysteme und Betriebssysteme für beschränkte Ressourcen
### Anforderungen an Betriebssysteme für eingebettete Systeme

* Aufgaben von Betriebssystemen jeder Art
	* Verwalten von Betriebsmitteln, Bereitstellen einer abstrakten Maschine, Verwalten der Kommunikation (Tasks, Prozesse, Rechner)
* Spezielle Anforderungen an Betriebssysteme für eingebettete Systeme
	* Konfigurierbarkeit
		* maßgerecht konfigurieren
			* verwenden von Modulen
			* Problem: validieren der vielen Varianten
	* oft: Echtzeitfähigkeit
		∗ Harte / weiche Echtzeiteigenschaften
			* Harte Zeitbedingung/Deadline
				* Ein Nachweis der Gültigkeit der Zeitbedingung ist erforderlich.
			* Weiche Zeitbedingung/Deadline
				* Ein Nachweis der Gültigkeit der Zeitbedingung ist nicht erforderlich, oder es ist nur eine statistische Zeitbedingung.
		∗ Determiniertheit
			* wichtig für Nachweis von Echtzeiteigenschaften 
			* insbesondere zeitliche Determiniertheit
		∗ Ereignisgesteuerte vs. zeitgesteuerte Systeme
			*	Zeitgesteuert: 
				* kompletter Schedule vorab berechnet
				* Hardware-Timer löst periodisch Ticks aus Scheduler wird genau durch Ticks geweckt und aktiviert dann neue Jobs
				* ggf. auch präemptiver Wechsel von Jobs möglich
				* benötigt alle parameter der jobs
				* unflexibel, schlechte prozessorausnutzung
				* wenig rechenaufwand zur laufzeit
				* leichter nachweis der zeitbedingung
				* für harte echtzeit meist verwendet
			* Ereignisgesteuerte
				* Verarbeitung bei Ereignis, bei ändernden Umweltgrößen oder Nachricht
				* Läuft so lange wie nötig
				* Währenddessen keine andere Verarbeitung, Interrupts möglich
				* Zu viele nachrichten-> Warteschlange
				* Volle Prozessorausnutzung bei hoher Last
				* Nachteil: Langer Rückstau bei vielen Nachrichten (Warteschlange)
				* Weiche Echtzeit, da Nachweis schwierig
		∗ oft: Unterbrechungstransparenz
		*	zur Laufzeit
			* nur bei ereignisgesteuerten Systemen, nicht bei zeitgesteuerten Systemen nötig
		* blockierende Synchronisation 
			* Semaphore oder Mutexe
			* Gefahr von Deadlocks
			* Gefahr der Prioritätsumkehr
		* nicht-blockierende Synchronisation
			* ergibt Unterbrechungstransparenz
			* Idee 
				* Task arbeitet auf Kopie
				* Ende atomar Kopie vergleichen und rückschreiben, wenn unverändert RISC: „compare and swap“ (CAS),CISC: „load-linked/store-conditional (LL/SC)
				* anderenfalls: alles nochmal versuchen
		∗ oft: Wiedereintrittsfähigkeit
			* „re-entrant code“
			* wichtig bei von Interrupt-Handlern aufgerufenem Code 
			* Lösung: alle Daten auf Stack
		∗ oft: Verdrängbarkeit
			* „pre-emptive code“
			* wichtig für schnelle Reaktionen bei Ereignissen hoher Priorität
	* oft: sparsam mit Betriebsmitteln 
		* langsamer Prozessor
		* wenig Speicher
		* keine HD
		* hohe Stückzahl
	* oft: direkter Zugriff auf Hardware 
		* effizienter Zugriff möglich
		* keine Schutzmechanismen notwendig
		* Zugriff auf Ein/Ausgabe
		* Interrupts
	* oft: erhöhte Zuverlässigkeit
		* falls ein sicherheitsrelevantes System
		* z.B. durch hoffentlich fehlerarmen 
		* Mikro-Kernel z.B. durch Software-Watchdog

### Architekturen von Betriebssystemen für eingebettete Systeme
* Monolithische und Schichtenarchitekturen 
	* intern in Hierarchie von festen Schichten aufgeteilt
	* innerhalb einer Schicht fein konfigurierbar
	* meist Trennung
		* Betriebssystem: privilegierter Mode 
		* Anwendungsprogramm: User-Mode 
		* Wechsel per Trap
	* LynxOS eCos AMX Nucleus OS OS-9 RTMX
* Client-Server-Architekturen
	* meiste Funktionalität:in User-Mode-Prozesse/Tasks verlagert
	* Mikro-Kernel für Grundlegenstes (insb. Interprozesskommunikation) im privilegierten Mode
	* Interprozesskommunikation per Message-Passing
	* klare Struktur erleichtert Wartung
	* Zuverlässigkeit durch Trennung erhöht
	* Message-Passing: in den letzten Jahren effizienter geworden
	* QNX (VxWorks) L4
* Virtuelle Maschinen
	* nur Monitor für virtuelle Maschinen auf realer Hardware
	* beliebige virtuelle Maschinen darauf, mit ggf. verschiedenen Betriebssystemen
	* damit Echtzeitsystem und Desktop-System auf derselben Hardware möglich
	* Monitor: bietet Synchronisation und Kommunikation partitioniert System-Betriebsmittel
	* normale Betriebssysteme ohne Modifikation verwendbar
	* ganzes Betriebssystem läuft im User-Mode, Monitor fängt privilegierte Befehle ab
	* ebenso für Interrupts: Monitor fängt sie ab und leitet sie weiter
	* kleiner Monitor: gut für Sicherheit und Zuverlässigkeit
* Doppel-Kernel-Ansatz
	* ähnlich zu virtuellen Maschinen
	* ebenfalls Echtzeitsystem und Desktop-System auf derselben Hardware möglich
	* Anpassung der Betriebssystem-Kernels an Monitor
	* Modifikation der Betriebssystem-Kernels nötig
	* TwinCAT, Simatic WinAC RTX, RTAI, RTMS, Xenomai
* CPU-Reservierung
	* Idee: Reservieren eines Cores eines Multi-Core-Prozessors für den Echtzeit-Prozess
	* nur ein Betriebssystem
	* Modifikation des Betriebssystem-Kernels nötig (Scheduling, CPU-Interrupt-Zuordnung, . . . )

### Beispiele für konkrete Betriebssysteme
* QNX
	* Client-Server-Architektur mit Mikro-Kernel („Neutrino“)
	* kleine Systeme mit knappen Betriebsmitteln (insb. Speicher) bis große, verteilte Echtzeitsysteme
	* eigener Adressraum für jedes Programm 
	* Multi-Threading möglich  
	* Programmierschnittstelle: POSIX-konform
	* Scheduling: prioritätsbasiert
		* FIFO
		* Round-Robin
		* Adaptive Scheduling
		* Aperiodisches Scheduling (dort „sporadic“ genannt)
	* im allg. präemptives Scheduling
	* Verfahren pro Prozess für alle seine Threads wählbar
	* ereignisgesteuert
	* harter Echtzeitbetrieb daher schwierig
	*  Thread-Scheduling nicht zeitgesteuert
	* periodische Threads per Zeitgeberfunktion möglich
* VxWorks 
	* Mikro-Kernel-basiert
	* ereignisgesteuert -> harter Echtzeitbetrieb daher schwierig
	* prozedurorientiert
	* Thread-Scheduling nicht zeitgesteuert
	* periodische Threads per Zeitgeberfunktion möglich
* RTAI
	* ähnlich Doppel-Kernel-Ansatz
	* Open-Source
	* jedes Betriebssystem in eigener „Domain“, aber mit vollem Hardware-Zugriff
	* erlaubt kontrolliertes Teilen von Betriebsmitteln zwischen Betriebssystemen
	* volle Kontrolle der Interrupt-Steuerung durch „Interrupt-Pipeline“
	

## Mehrere Systeme auf nur einem Rechner
### Integrated Modular Avionics (IMA) für  Flugzeuge
* Überblick über Integrated Modular Avionics
	* gemeinsame Hardwarenutzung:
		* möglich durch immer schnellere Rechner
		* spart Gewicht im Luftfahrzeug
	* gleichartige Rechner-Module
		* spart Entwicklung und Lagerhaltung -> kosten
	* ![bla](IMA.png)
	* Zusammenfassung: 
		*  wenige, gleichartige, standardisierte Rechner-Module
		* 1 standardisierter Bustyp (schnell, echtzeitfähig) 
		* standardisiertes IMA-Betriebssystem (mit Partitionierung)
* AFDX Daten-Netzwerk
	*„Avionics Full DupleX Switched Ethernet“
	* 100 Mbit/s Ethernet
	* kompatibel zu Schicht 1 und 2 von konventionellem Ethernet (billige, fertig käufliche Hardware dafür verwendbar)
	* pro Flugzeug zwei unabhängige Netzwerke für Zuverlässigkeit durch Redundanz
	* echtzeitfähig
	* realisiert durch feste Sendezeit-Slots und feste Routen
	* Routingtabelle jeweils fest in Switch eingebrannt
* Betriebssystemschnittstelle ARINC 653
	* Betriebssystem sorgt für  Partitionierung
	* API standardisiert
	* zwei Arten von Partitionen:
		* Anwendungs-Partition
		* System-Partition (Verwaltung des IMA-Moduls, Health-Monitoring)
	* innerhalb Partition: ggf. mehrere „Prozesse“
* Erweiterung/Forschung: Distributed Modular Electronics (DME)
	* IMA: Sensoren/Aktuatoren jeweils fest mit 1 IMA-Modul verdrahtet
	* DME: Trennen des Rechnens von den Sensor-/Aktuator-Schnittstellen
	* ![awef](dme.png)
	* CPM=Core Prozessing module,RDC:Remote Data Concentrator(input), RPC: Remote Power Controller
	* alles verbunden mit 2 redundanten AFDX-Netzwerk-Strängen
	* nun evtl. möglich:  dynamisches Ersetzen defekter CPMs durch Rekonfiguration
  
### Systeme mit gemischter Dependability
* Der Begriff Dependability
	* Verlässlichkeit: Das System versagt nicht häufiger und schlimmer als akzeptabel
	* availability: readiness for correct service. 
	* reliability: continuity of correct service.
	* safety: absence of catastrophic consequences on the user(s) and the environment.
	* integrity: absence of improper system alterations. maintainability: ability to undergo modifications and repairs.
	* ist übergreifendes Konzept mit mehreren Attributen
* Problem bei gemischter Dependability
	* für alle Funktionalitäten: Stufe des Aufwands für Qualitätsnachweis = Stufe der kritischsten Funktionalität
	* hohe Mehrkosten bei Entwicklung und SW-Wartung, falls viele Funktionalitäten auf einem Rechner, die sich alle gegenseitig beeinträchtigen können
* Lösungsansätze: Separation Kernel, Virtualisierung
	* Separation Kernel
		* Art von Betriebssystemen
		* anwendung denkt hätte kernel alleine
		* Aufwand für Qualitätsnachweis: nur wie für Anwendung nötig
		* Aufwand für Qualitätsnachweis des Betriebssystems nur einmal nötig
		* PikeOS, VxWorks 653
	* Virtualisierung
		* Typ 1 Hypervisor, teilt Rechenzeit und Hauptspeicher auf Partitionen auf.
		* statisches, zyklisches Scheduling der Partitionen (üblicherweise) 
		* Zugriff auf Hauptspeicherbereich: nur durch zugehörige Partition 
		* ggf. auch Zugriff auf Peripherie: nur durch je eine Partition

### Weitere Lösungen für einige weitere Anwendungsbereiche

* IMA-SP: Anpassung von IMA an Weltraumfahrt
	* Integrated Modular Avionics for Space
	* langsamere Prozessoren wg. Strahlung 
	* weniger komplexe Systeme
	* + Raumfahrt-spezifischer Anforderungen
	* - Kommunikation über AFDX
	* Sehr enger Anwendungsbereich
	* Gemeinsamkeiten von Luft- und Raumfahrt anscheinend nicht explizit untersucht
	* Launcher-Eigenheiten ignoriert
* AutoSAR: IMA-ähnlicher Ansatz in der Automobilindustrie
	* für Kraftfahrzeuge


### Einige offene Herausforderungen
* bei Partitionierung der Rechenzeit 
	* CPUs mit mehreren Kernen
		* Abhängigkeit bei Rechenzeit zwischen den Kernen 
		* Caches abschalten? – evtl. langsamer als nur 1 Kern
	* Direkter Speicherzugriff (DMA)
		* DMA-Controller konkurriert mit CPU um Speicherbus -> Echtzeit-Partition ausgebremst
* bei Echtzeitnachweisen
	* Worst-Case-Leistung und Prozessorarchitektur 
	* Timing-Anomalien und Prozessorarchitektur

