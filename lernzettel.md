# Reaktive Systeme
* Einordnung reaktiver Systeme
	* transformatorische Systeme 
		* Sortierprogramm 
		* überführen eine Menge von Eingabedaten in Ausgabedaten
und terminieren dann
	* interaktive Systeme
		* 
Textverarbeitung, Datenbankserver
		* reagieren kontinuierlich mit ihrer Umgebung
		* können den Ablauf weitgehend selbst kontrollieren
		* Reaktionen können durch Ressourcenengpässe verzögert werden
	* reaktive Systeme
		* Motorsteuerung
		* reagieren kontinuierlich mit ihrer Umgebung
		* Ablauf und Tempo durch die Umgebung kontrolliert
* Charakterisierung von Arten reaktiver Systeme
	* diskrete Anwendungen
		* reagieren auf diskrete Eingangsereignisse
		* produzieren diskrete Ausgangsereignisse
		* Betätigen eines Schalters 
		* Ausschalten eines Motors
		* kontroll(fluss)orientiert / zustandsorientiert
	* kontinuierliche Anwendungen
		* produzieren kontinuierlich Ausgangsdaten
		* aus kontinuierlich eintreffenden Eingangsdaten
		* signalverarbeitende Systeme
		* daten(fluss)orientiert
* Besondere Herausforderungen bei reaktiven Systemen für diskrete Anwendungen 
	* Nebenläufigkeit
		* Ein- und Ausgabegrößen gleichzeitig bedienen
		* Nur 1 Prozessor
	* Abbruch
		* Zu beobachtende Abbruchbedingung
		* Für "dauernd" laufendes System
		* Problem: konsistenter Zustand bei Abbruch
	* Unterbrechung
		* Muss aktiv für Dringendes unterbrochen werden
		* Problem: Wiederherstellung und Nebenläufigkeit
	* Race Conditions
		* Folge der Nebenläufigkeit
		* Folge: nicht-deterministisches Verhalten
		* Semaphore/Monitore verhindern dies
* Ein Muster für einen Lösungsansatz in Software
	* Eingabewerte ermitteln
und in internen Eingabevariablen ablegen
	* interne Ausgabevariablen anhand der (nun festen) Eingabevariablen berechnen
	* interne Ausgabevariablen ausgeben
	* Vorteile: 
		* Reihenfolge der Berechnung der internen Ausgabevariablen egal
		* keine verschränkten Zugriffe
		* konsistenter Zustand bei Abbruch (Run-to-Completion-Semantik nach Eingabe-Ereignis)
		* durch gröbere Zeitgranularität weniger Nicht-Determinismus
	* Beachten: 
		* Berechnungen müssen schnell sein
		* immer noch Nicht-Determinismus
* Auffrischung: Endliche Automaten / Schaltwerke
		* Schaltnetz
			* Die Ausgabevariablen hängen von den aktuellen Eingabevariablen ab.
		* Schaltwerk
			* Die Ausgabevariablen hängen von den aktuellen Eingabevariablen und deren Vorgeschichte ab.
	* Modellieren von Schaltwerken 
		* Endliche Automaten
			* ![Alternativer Text](eauto.png)

		* Zustände eines Systems
			* Zustände der Eingabevariablen
			* Zustände des zu bauenden Systems
			* Zustände der Ausgabevariablen
	* Implementieren von Schaltwerken
		* Huffman-Normalform?
 	* Mehr zur Darstellung von Modellen von Schaltwerken 
 		* Mealy-Automat
			* ein Endlicher Automat mit:
 			* f:Z×X→Y
 		* Moore-Automat
			* ein Endlicher Automat mit:
			* f:Z→Y
			* Ausgabefunktion f : hängt nicht von Eingabe ab
		* Darstellungsformen für Automaten
			* Zustandsgraph
				* Mealy-Typ
					* ![Alternativer Text](Mealy-Typ.png)
				* Moore-Typ
					* ![Alternativer Text](Moore-Typ.png)
 
			* Tabelle
				* ![Alternativer Text](tabellen.png)
			* „regulärer Ausdruck“, . . . 
			* spezielle VHDL-Konstrukte
			* Programmcode, z.B. in C
 	* Ein wenig zum Testen von Schaltwerken
 		* Testfolge
			* eine geordnete Liste von Paaren aus Eingabevektor ⃗x und erwartetem Ausgabevektor ⃗y

				* Eingabevektoren: nacheinander an Testling angelegt 
				*erwartete Ausgabevektoren: jeweils erwartete Reaktion
 	* __TODO__ Übungsblatt 1: Testfolge für endlichen Automaten

# Echtzeitsysteme
## Einleitung
* Was bedeutet „Echtzeit“?
	* nicht: „besonders schnell“ 
 	* nur: „sicher rechtzeitig“
	* daher meist: ziemlich langsam -> b/c Prozessorauslastung

* Worst-Case-Ausführungszeit
	* Worst-Case-Ausführungszeit
		* die Programmlogik (Kontrollflussgraph),
		* die Eingabedaten (Auswirkungen auf Schleifendurchlaufzahlen etc.),
		* den Compiler (Optimierungsstufe) und
		* die Architektur und Taktfrequenz des Ausführungsrechners (Ausführungsgeschwindigkeit unter Berücksichtigung von Cache- und Pipelining-Effekten).
	* bestimmbar: obere Schranke der WCET angeben
## Definitionen der Grundbegriffe
### Jobs und Prozessoren
* Definition Job und Task 
	* Job: 
		* Eine Aufgabe, die als Einheit vom System eingeplant und ausgeführt wird.
		* Bsp: Zustandsübergang Endlicher Automat, Übertragung Datenpaket
	* Task:
		* Eine Menge zusammengehöriger Jobs, die eine Systemfunktion realisiert
		* regelmäßige Übertragung von Datenpaketen
* Definition Prozessor
	* Prozessor
		* Eine Ressource, die für die Ausführung eines Jobs benötigt wird, und die bei der Ausführung des Jobs Zeit verbraucht.
		* BSP: CPU, Netzwerk, HDD

### Freigabezeitpunkt, Periode, Phase
* Definition Freigabezeitpunkt eines Jobs
	* Freigabezeitpunkt (release time)
	* Der Zeitpunkt, ab dem der Job ausgeführt werden kann.
	* BSP
		* ein Regler für einen Ofen
		* Ausführung startet nach Initialisierung zur Zeit 0 s
* Definition Periode und Phase eines Tasks
	* Periode
		* Der Zeitabstand zwischen den Freigabezeitpunkten der Jobs des Tasks.
	* Phase
 		* Der Freigabezeitpunkt des ersten Jobs des Tasks.

### Deadline, Zeitbedingung
* Definition Deadline eines Jobs
	* Der Zeitpunkt, zu dem die Ausführung des Jobs fertig gestellt sein muss.
* Weitere Definitionen
	* Antwortzeit (response time)
		* Die Dauer vom Freigabezeitpunkt bis zur Fertigstellung.
	* relative Deadline
		* Die maximal zulässige Antwortzeit.
	* absolute Deadline
		* Der Freigabezeitpunkt plus die relative Deadline.
	* Zeitbedingung (timing constraint)
		* Eine Bedingung für das Zeitverhalten des Jobs.

### Harte und weiche Zeitbedingungen
* Definition über die Kritikalität
	* Harte Zeitbedingung/Deadline
		* Katastrophale Folgen, wenn nicht erfüllt.
	* Weiche Zeitbedingung/Deadline
		* Unerwünschte Folgen, wenn nicht erfüllt.	
* Definition über den Nutzen verspäteter Ergebnisse
	* Verspätung eines Jobs (tardiness)
		* falls zu spät: Fertigstellungszeitpunkt minus absolute Deadline sonst: Null
	* Harte Zeitbedingung/Deadline
		* Nutzen sinkt schlagartig, falls Verspätung > 0 (Nutzen kann sogar stark negativ werden)
	* Weiche Zeitbedingung/Deadline
		* Nutzen sinkt (graduell) um so mehr, je größer Verspätung
* Definition über deterministische vs. probabilistische Zeitbedingungen 
	* Harte Zeitbedingung/Deadline 
		* Job darf nie Deadline verpassen.
	* Weiche Zeitbedingung/Deadline
		*Job darf Deadline nur mit bestimmter, niedriger Wahrscheinlichkeit verpassen.
* Definition über den Nachweis der Gültigkeit (nach Liu)
	* Harte Zeitbedingung/Deadline
		* Ein Nachweis der Gültigkeit der Zeitbedingung ist erforderlich.
	* Weiche Zeitbedingung/Deadline
		* Ein Nachweis der Gültigkeit der Zeitbedingung ist nicht erforderlich, oder es ist nur eine statistische Zeitbedingung.

## Gesamtmodell eines Echtzeitsystems
* Lastmodell -> was? 
* Ressourcen-Modell -> womit?
* Scheduling-Algorithmus -> wann?

### Lastmodell
* Zeiteigenschaften von Lasten
	* Jitter des Freigabezeitpunkts
		* Wenn genauer Freigabezeitpunkt unbekannt: Zeitspanne, in der die Freigabe geschieht
	* Aperiodischer Job
		* Konkreter Freigabezeitpunkt ist komplett unbekannt, und Deadline ist weich.
	* Sporadischer Job
		* Konkreter Freigabezeitpunkt ist komplett unbekannt, und Deadline ist hart.
* Einige weitere Eigenschaften von Lasten
### Ressourcen-Modell
* Prozessoren und Ressourcen * Eigenschaften von Ressourcen
### Scheduling-Algorithmus
* Korrekter Schedule
* Durchführbarer Schedule und optimaler Scheduling-Algorithmus
## Häufig verwendete Ansätze für Echtzeit-Scheduling
### Zeitgesteuerte Verarbeitung
* Einführung: Zeitgesteuerte Verarbeitung * Round-Robin-Verarbeitung
* Gewichtete Round-Robin-Verarbeitung
### Prioritätsgesteuerte Verarbeitung
* Einführung: Prioritätsgesteuerte Verarbeitung * Beispiele für prioritätsgesteuerte Algorithmen * Earliest Deadline First (EDF) Algorithmus

* Least Slack Time (LST) Algorithmus * Scheduling ohne/mit Präemption
* Scheduling auf mehrere Prozessoren * Scheduling mit Migration
* Übungsblatt 2: Scheduling: Nicht-Optimalität von LST bei Konflikten
### Verhalten prioritätsgesteuerter Systeme
* Beispiel für anormales Verhalten prioritätsgesteuerter Systeme
### Weitere Aspekte
* Scheduling unter Überlast
* Scheduling mit Ressourcen-Restriktionen

## Nachweis harter Echtzeiteigenschaften
* Der Begriff der längstmöglichen Antwortzeit
* Praktische Bestimmung der längstmöglichen Ausführungszeit
* Praktische Bestimmung der längstmöglichen Antwortzeit
* Laboraufgabe 2: Nachweis harter Echtzeiteigenschaften (der Fahrstuhlsteuerung)

# Software-Engineering für eingebettete Systeme
## Modularisierung und Geheimnisprinzip
### Der Begriff des Moduls
* Schreibzeitmodule vs. Linkzeitmodule
* Was bestimmt dabei jeweils die Aufteilung in Module
* Folgen der Vermischung der verschiedenen Arten von Modulen

### Beispiel: Zerlegung eines KWIC-Systems in Module
* Einführung in die KWIC-Aufgabe
* Ideen für eine Zerlegung in Module
* Eine „konventionelle“ Zerlegung in Module * Einige wahrscheinliche Änderungen
* Parnas’ Zerlegung in Module
* Vergleich der beiden Zerlegungen in Module
3.3.3 Kriterien für ein Zerlegen in Module
* Die Kriterien
* Das Geheimnis eines Moduls
* Einige konkrete Kriterien
* Schnittstellen zwischen Modulen
* Modulstruktur
* Übungsbeispiel: Projekt Sichere Eisenbahnsteuerung
3.3.4 Diskussion des Ansatzes
„Laufzeiteffizienz“ und „Vergleich . . . “ kommen in der Klausur nicht vor, da sie aus Zeitgründen nicht mehr behandelt wurden.
* Einordnung in die Veranstaltung und in den Studiengang


4 Sicherheitsrelevante Systeme (im Sinne von Safety)
4.2 Methoden zur Risikoanalyse
4.2.1 Grundbegriffe
* Begriffsdefinitionen
* Hierarchische Betrachung
4.2.2 Risikograf
* Risikograf nach nach EN 954-1
4.2.3 FMEA

4.2.5 Fehlerbaumanalyse (FTA)
* Grundidee
* Begriffsdefinitionen
* verschiedene Arten von Syntax für eine FTA
* Beispiel: Bremsflüssigkeitswarnlampensystem
* Fehlerbäume mit Wahrscheinlichkeiten
* Übungsblatt 4: Fehlerbaumanalyse
* Laboraufgabe 6: Risikoanalyse und zuverlässige Systemarchitektur am Beispiel des Fahrradtachome-
ters

4.3 Hardware- und Software-Systemarchitekturen
4.3.1 Sicherheitsarchitekturen
* 1-aus-1-Struktur
* Definition Hardware Fault Tolerance (HFT)
* 1-aus-2-Struktur
* 1-aus-2-D-Struktur (1-aus-2 mit Diagnose)
* Fail-Safe-Logik (für 1-aus-2-Struktur)
* 2-aus-3-Struktur
* Fehlerbaum mit mehrkanaligen Strukturen
4.3.2 Architekturen für Logikeinheiten
* Zentralprozessor mit Watchdog
* Sicherheitsüberwachung bzw. Sicherheitsschicht
* Zweikanaligkeit mit homogener Redundanz
* Einkanalige Struktur mit Softwarediversität
4.3.3 Beispiel: Projekt Sichere Eisenbahnsteuerung
* Einordnen der Architektur des Projekts
4.4 Wie sicher ist sicher genug?
4.4.1 Risiko-Nutzen-Analyse und die Alternativen
* Utilitaristischer Ansatz
* Risikoabschätzung
* Akzeptables Risiko
* Alternativer Ansatz „optimales Risiko“
* Alternativer Ansatz „normale Unfälle“
* Alternativer Ansatz „Unfallrate nicht erhöhen“
* Bewertung der Ansätze

5 Betriebssysteme für eingebettete Systeme

5.1 Echtzeit-Betriebssysteme und Betriebssysteme für beschränkte Ressourcen
5.1.1 Anforderungen an Betriebssysteme für eingebettete Systeme
* Übungsblatt 5: Abgrenzungen
* Dual-Use in der Informatik
* Entscheidungshilfen
* Aktuell: ethische Diskussion an der HSB
* Unvermeidlichkeit von Dilemmata im Berufsleben
* Aufgaben von Betriebssystemen jeder Art
– Verwalten von Betriebsmitteln, Bereitstellen einer abstrakten Maschine, Verwalten der Kom-
munikation
* Spezielle Anforderungen an Betriebssysteme für eingebettete Systeme
– Konfigurierbarkeit
– oft: Echtzeitfähigkeit
∗ Harte / weiche Echtzeiteigenschaften
∗ Determiniertheit
∗ Ereignisgesteuerte vs. zeitgesteuerte Systeme ∗ oft: Unterbrechungstransparenz
∗ oft: Wiedereintrittsfähigkeit
∗ oft: Verdrängbarkeit
– oft: sparsam mit Betriebsmitteln – oft: direkter Zugriff auf Hardware – oft: erhöhte Zuverlässigkeit
5.1.2 Architekturen von Betriebssystemen für eingebettete Systeme
* Monolithische und Schichtenarchitekturen * Client-Server-Architekturen
* Virtuelle Maschinen
* Doppel-Kernel-Ansatz
* CPU-Reservierung
5.1.3 Beispiele für konkrete Betriebssysteme
* QNX
* VxWorks * RTAI

5.2 Mehrere Systeme auf nur einem Rechner
5.2.1 Integrated Modular Avionics (IMA) für Flugzeuge
* Überblick über Integrated Modular Avionics
* AFDX Daten-Netzwerk
* Betriebssystemschnittstelle ARINC 653
* Erweiterung/Forschung: Distributed Modular Electronics (DME)
5.2.2 Systeme mit gemischter Dependability
* Der Begriff Dependability
* Problem bei gemischter Dependability
* Lösungsansätze: Separation Kernel, Virtualisierung
5.2.3 Weitere Lösungen für einige weitere Anwendungsbereiche
* IMA-SP: Anpassung von IMA an Weltraumfahrt
* AutoSAR: IMA-ähnlicher Ansatz in der Automobilindustrie
5.2.4 Einige offene Herausforderungen
* bei Partitionierung der Rechenzeit * bei Echtzeitnachweisen

